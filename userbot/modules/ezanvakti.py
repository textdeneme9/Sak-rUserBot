# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# SakirUserBot - SakirBey - Midy

# .
# .

ithalat  istekleri
dan  userbot  ithalat  CMD_HELP
dan  userbot . olayları  içe aktarma  kaydı
dan  BS4  ithalat  BeautifulSoup
dan  userbot . cmdhelp  içe aktarma  CmdYardım

# ██████ DİL SABİTLERİ ██████ #

dan  userbot . dil  içe aktarma  get_value
LANG  =  get_value ( "ezanvakti" )

# ████████████████████████████████ #


@ register ( giden = Doğru , desen = "^ .ezanvakti? (\ w *)" )
async  def  ezanvakti ( etkinlik ):
    Konum  =  olay . pattern_match . grup ( 1 ). daha düşük ()
    eğer  değil  olay . metin . bölüm ( konum ) [ 2 ] ==  '' :
        ilce  =  olay . metin . bölüm ( konum ) [ 2 ]
    başka :
        ilce  =  Yok

    Eğer  len ( Konum ) <  1 :
         olayı bekliyoruz . düzenle ( LANG [ 'NEED_CITY' ])
        dönüş

    URL  =  f'https: //www.mynet.com/ { Konum } '/ namaz-vakitleri
    eğer  değil  Ilce  ==  None :
        url  + =  '/'  +  ilce . şerit ()

    istek  =  istekler . get ( url )
    eğer  değil  talep . status_code  ==  200 :
         olayı bekliyoruz . düzenle ( f "" { konum }  { LANG [ 'NOT_FOUND' ] } `" )
        dönüş

    BS4  =  BeautifulSoup (
        istek . metin , 'lxml'
    )

    sonuç  =  bs4 . find ( 'div' , { 'class' : 'dua-zaman çizelgesi' }). find_all ( 'div' )
    imsak  =  sonuç [ 0 ]. bul ( 'span' , { 'class' : 'time' }). get_text (). şerit ()
    gunes  =  sonuç [ 1 ]. bul ( 'span' , { 'class' : 'time' }). get_text (). şerit ()
    ogle  =  sonuç [ 2 ]. find ( 'span' , { 'class' : 'time' }). get_text (). şerit ()
    ikindi  =  sonuç [ 3 ]. find ( 'span' , { 'class' : 'time' }). get_text (). şerit ()
    aksam  =  sonuç [ 4 ]. find ( 'span' , { 'class' : 'time' }). get_text (). şerit ()
    yatsi  =  sonuç [ 5 ]. bul ( 'span' , { 'class' : 'time' }). get_text (). şerit ()

    vakitler  = ( f "** { LANG [ 'DIYANET' ] } ** \ n \ n "  + 
                 f "📍 ** { DİL [ 'YER' ] } : ** ' { Konum . harf () } / { İlçe . şeridi . () yararlanmak () halinde  değil  İlçe  ==  Yok  başka  Konum . harf () } ' \ n \ n "  +
                 f "🏙 ** { LANG [ 'SANSAK' ] } : **` ` { imsak } ` \ n "  +
                 f "🌅 ** { LANG [ ' SİLAHLAR ' ] } : **` ` { güneş } ` \ n "  +
                 f "🌇 ** { LANG [ 'OGLE' ] } : **" { ogle } " \ n "  +
                 f "🌆 ** { LANG [ 'IKINDI' ] } : **" { ikindi } " \ n "  +
                 f "🌃 ** { LANG [ 'AKSAM' ] } : **` { aksam } ` \ n "  +
                 f "🌌 ** { LANG [ 'YATSI' ] } : **" { yatsi } " \ n " )

     olayı bekliyoruz . düzenle ( vakitler )

CmdHelp ( 'ezanvakti' ). add_command (
    'ezanvakti' , '<şehir> <ilçe>' , 'Belirtilen şehir için namaz vakitlerini gösterir.' , 'ezanvakti ankara etimesgut'
). ekle ()