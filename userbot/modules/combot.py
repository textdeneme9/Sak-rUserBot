# SakirUserBot - SakirBey - Berceste

dan  Yardım kampanyasının . içe aktarma hataları  ( ChannelInvalidError , ChannelPrivateError , ChannelPublicGroupNaError )
dan  emojisi  ithalat  emojize
dan  Yardım kampanyasının . tl . türler  MessageActionChannelMigrateFrom , ChannelParticipantsAdmins'i içe  aktarır
dan  Yardım kampanyasının . tl . fonksiyonlar . iletiler  GetHistoryRequest , GetFullChatRequest'i içe  aktarır
dan  userbot . olayları  içe aktarma  kaydı
dan  tarih saat  ithalat  datetime
dan  matematik  ithalat  sqrt
dan  Yardım kampanyasının . tl . fonksiyonlar . kanallar  GetFullChannelRequest , GetParticipantsRequest'i içe  aktarır
dan  Yardım kampanyasının . yardımcı programları  içe aktar  get_input_location
dan  userbot . cmdhelp  içe aktarma  CmdYardım

# ██████ DİL SABİTLERİ ██████ #

dan  userbot . dil  içe aktarma  get_value
LANG  =  get_value ( "birleşik" )

# ████████████████████████████████ #


# Https://github.com/alcyper/alcyper'DEN FORKED #
@ register ( outgoing = True , pattern = "^ .stats (?: | $) (. *)" )
eşzamansız  tanım  bilgisi ( olay ):
     olayı bekliyoruz . düzenle ( LANG [ "ANALIZ" ])
    chat  =  get_chatinfo ( olay ) bekleyin 
    caption  =  fetch_info bekliyor  ( sohbet , etkinlik )
    dene :
         olayı bekliyoruz . düzenle ( caption , parse_mode = "html" )
     e gibi İstisna  hariç : 
        print ( "İstisna:" , e )
         olayı bekliyoruz . düzenle ( f'` { LANG [ "HATA" ] }  { e } "' )
    dönüş
    
    
eşzamansız  def  get_chatinfo ( olay ):
    chat  =  olay . pattern_match . grup ( 1 )
    chat_info  =  Yok
     sohbet ederseniz :
        dene :
            chat  =  int ( sohbet )
         ValueError dışında :
            geçmek
    eğer  değil  sohbet :
        eğer  olay . response_to_msg_id :
            replied_msg  =  bekliyoruz  olay . get_reply_message ()
            eğer  replied_msg . fwd_from  ve  replied_msg . fwd_from . channel_id  olduğunu  değil  Hiçbiri :
                chat  =  replied_msg . fwd_from . channel_id
        başka :
            chat  =  olay . chat_id
    dene :
        chat_info  =  bekliyoruz  olay . istemci ( GetFullChatRequest ( sohbet ))
    hariç :
        dene :
            chat_info  =  bekliyoruz  olay . istemci ( GetFullChannelRequest ( sohbet ))
         ChannelInvalidError dışında :
             olayı bekliyoruz . yanıt ( DİL [ "GEÇERSİZ" ])
            dönüş  Yok
         ChannelPrivateError dışında :
             olayı bekliyoruz . yanıtla ( LANG [ "ERROR_BAN" ])
            dönüş  Yok
         ChannelPublicGroupNaError dışında :
             olayı bekliyoruz . yanıtla ( LANG [ "NOT_FOUND" ])
            dönüş  Yok
        hariç ( TypeError , ValueError ) olarak  err :
             olayı bekliyoruz . cevap ( str ( err ))
            dönüş  Yok
    dönüş  chat_info


eşzamansız  def  fetch_info ( sohbet , etkinlik ):
    # chat.chats bir listedir, bu yüzden IndexError'dan kaçınmak için get_entity () kullanıyoruz
    chat_obj_info  =  bekliyoruz  olay . müşteri . get_entity ( sohbet . full_chat . id )
    yayın  =  chat_obj_info . yayın  if  hasattr ( chat_obj_info , "yayın" ) başka  Yanlış
    chat_type  =  "Kanal"  ,  başka bir yayın  ise "Grup" 
    chat_title  =  chat_obj_info . Başlık
    warn_emoji  =  emojize ( ": uyarı:" )
    dene :
        msg_info  =  await  olayı . istemci ( GetHistoryRequest ( eş = chat_obj_info . id , offset_id = 0 , offset_date = datetime ( 2010 , 1 , 1 ),
                                                        add_offset = - 1 , limit = 1 , max_id = 0 , min_id = 0 , hash = 0 ))
     e gibi İstisna  hariç : 
        msg_info  =  Yok
        print ( "İstisna:" , e )
    # Önce msg_info.messages'ı kontrol ettiği için IndexError şansı yok
    first_msg_valid  =  Doğru  ise  msg_info  ve  msg_info . mesajlar  ve  msg_info . mesajlar [ 0 ]. id  ==  1  başka  Yanlış
    # Msg_info.users için aynı
    creator_valid  =  first_msg_valid ve msg_info ise true  . başka kullanıcılar Yanlış     
    creator_id  =  msg_info . kullanıcılar [ 0 ]. id  eğer  creator_valid  başka  Yok
    creator_firstname  =  msg_info . kullanıcılar [ 0 ]. creator_valid ise first_name  ve msg_info . kullanıcılar [ 0 ]. first_name olduğu değil Yok başka "Silinmiş Hesap"        
    creator_username  =  msg_info . kullanıcılar [ 0 ]. creator_valid ve msg_info ise kullanıcı adı  . kullanıcılar [ 0 ]. kullanıcı adı olduğunu değil Yok başka Yok        
    oluşturuldu  =  msg_info . mesajlar [ 0 ]. tarih  if  first_msg_valid  else  None
    previous_title  =  msg_info . mesajlar [ 0 ]. eylem . Başlıktaki  halinde  first_msg_valid  ve  tipi ( msg_info . mesajları [ 0 ]. aksiyon ) olan  MessageActionChannelMigrateFrom  ve  msg_info . mesajlar [ 0 ]. eylem . title  ! =  chat_title  else  Yok
    dene :
        dc_id , location  =  get_input_location ( sohbet . full_chat . chat_photo )
     e gibi İstisna  hariç : 
        dc_id  =  "Bilinmeyen"
        konum  =  str ( e )
    
    # bu değiştirmem gereken biraz spagetti
    açıklama  =  sohbet . full_chat . hakkında
    üyeler  =  sohbet . full_chat . Katılımcılar_sayısı  eğer  hasattr ( chat . full_chat , " icip_count " ) else  chat_obj_info . Katılımcılar_sayısı
    yöneticiler  =  sohbet . full_chat . admins_count  if  hasattr ( chat . full_chat , "admins_count" ) else  Yok
    banned_users  =  sohbet . full_chat . kicked_count  if  hasattr ( chat . full_chat , "kicked_count" ) else  Yok
    restrcited_users  =  sohbet . full_chat . banned_count  if  hasattr ( chat . full_chat , "banned_count" ) else  Yok
    members_online  =  sohbet . full_chat . online_count  if  hasattr ( chat . full_chat , "online_count" ) else  0
    group_stickers  =  sohbet . full_chat . çıkartma seti . başlık  if  hasattr ( sohbet . full_chat , "stickerset" ) ve  sohbet edin . full_chat . stickerset  else  Yok
    messages_viewable  =  msg_info . saymak  eğer  msg_info  başka  Yok
    messages_sent  =  sohbet . full_chat . read_inbox_max_id  if  hasattr ( chat . full_chat , "read_inbox_max_id" ) else  Yok
    messages_sent_alt  =  sohbet . full_chat . read_outbox_max_id  if  hasattr ( chat . full_chat , "read_outbox_max_id" ) else  Yok
    exp_count  =  sohbet . full_chat . pts  if  hasattr ( sohbet . full_chat , "pts" ) else  Yok
    kullanıcı adı  =  chat_obj_info . username  if  hasattr ( chat_obj_info , "username" ) else  Yok
    bots_list  =  sohbet . full_chat . bot_info   # bu bir listedir
    botlar  =  0
    supergroup  =  "✅"  if  hasattr ( chat_obj_info , "megagroup" ) ve  chat_obj_info . megagroup  başka  "❎"
    slowmode  =  "✅"  if  hasattr ( chat_obj_info , "slowmode_enabled" ) ve  chat_obj_info . slowmode_enabled,  başka  "❎"
    slowmode_time  =  sohbet . full_chat . slowmode_seconds  if  hasattr ( chat_obj_info , "slowmode_enabled" ) ve  chat_obj_info . slowmode_enabled  else  Yok
    sınırlı  =  "✅"  ise  hasattr ( chat_obj_info , "kısıtlı" ) ve  chat_obj_info . kısıtlanmış  başka  "❎"
    doğrulanmış  =  "✅"  eğer  hasattr ( chat_obj_info , "doğrulanmış" ) ve  chat_obj_info . başka doğrulandı  "❎" 
    kullanıcı adı  =  "@ {}" . kullanıcı adı başka ise format ( kullanıcı adı ) Yok   
    creator_username  =  "@ {}" . format ( creator_username ) eğer  creator_username  else  Yok
    # spagetti bloğunun sonu
    
    eğer  yöneticileri  olduğunu  Hiçbiri :
        # chat.full_chat.admins_count Yok ise, yönetici olmadan da çalışıyorsa bu alternatif yolu kullanın
        dene :
            katılımcılar_admins  =  olayı bekliyor  . istemci ( GetParticipantsRequest ( channel = chat . full_chat . id , filter = ChannelParticipantsAdmins (),
                                                                            ofset = 0 , limit = 0 , hash = 0 ))
            yöneticiler  =  katılımcılar_yöneticiler . katılımcılar_yöneticiler başka değilse say  Yok   
         e gibi İstisna  hariç : 
            print ( "İstisna:" , e )
    eğer  bots_list :
         bot_list içindeki bot  için : 
            botlar  + =  1

    başlık  =  f '<b> { LANG [ "IST" ] } </b> \ n '
    başlık  + =  f "🆔: <code> { chat_obj_info . id } </code> \ n "
    eğer  messages_viewable  olduğunu  değil  Hiçbiri :
        başlık  + =  f ' { LANG [ "GÖR" ] } : <code> { messages_viewable } </code> \ n '
    eğer  messages_sent :
        başlık  + =  f ' { LANG [ "GOND" ] } : <code> { messages_sent } </code> \ n '
    elif  messages_sent_alt :
        başlık  + =  f ' { LANG [ "GOND" ] } : <code> { messages_sent_alt } </code> { warn_emoji } \ n '
    dönüş  başlığı
    
CmdHelp ( 'combot' ). add_command ( 'istatistik' , Yok , 'Mesaj geliş (combot gibi).' ). ekle ()