# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# SakirUserBot - SakirBey - Midy

# ██████ DİL SABİTLERİ ██████ #

dan  userbot . dil  içe aktarma  get_value
LANG  =  get_value ( "covid19" )

# ████████████████████████████████ #

dan  userbot  ithalat  CMD_HELP
dan  userbot . olayları  içe aktarma  kaydı
dan  istekler  ithalat  get
ithal  pytz
ithalat  bayrağı
dan  userbot . cmdhelp  içe aktarma  CmdYardım

@ register ( outgoing = True , pattern = "^ .covid? (. *) $" )
async  def  covid ( olay ):
    dene :
        eğer  olay . pattern_match . grup ( 1 ) ==  '' :
            ülke  =  'TR'
        başka :
            ülke  =  olay . pattern_match . grup ( 1 )

        bayrak  =  bayrak . bayrak ( ülke )
        worldData  =  get ( 'https://coronavirus-19-api.herokuapp.com/all' ). json ()
        countryData  =  get ( 'https://coronavirus-19-api.herokuapp.com/countries/'  +  pytz . country_names [ ülke ]). json ()
    hariç :
         olayı bekliyoruz . düzenle ( LANG [ 'SOME_ERRORS' ])
        dönüş

    sonuclar  = ( f "** { LANG [ 'DATA' ] } ** \ n "  +
                f " \ n ** { LANG [ 'EARTH' ] } ** \ n "  +
                f "** { LANG [ 'CASE' ] } **` ` { worldData [ ' case ' ] } " \ n "  +
                f "** { LANG [ 'ÖLÜM' ] } **` ` { worldData [ 'ölümler' ] } ` \ n "  +
                f "** { LANG [ 'HEAL' ] } **" { worldData [ 'kurtarıldı' ] } " \ n "  +
                f " \ n ** { pytz . country_names [ ülke ] } ** \ n "  +
                f "** { bayrak }  { LANG [ 'TR_ALL_CASES' ] } **` ` { countryData [ 'vakalar' ] } ` \ n "  +
                f "** { bayrak }  { LANG [ 'TR_CASES' ] } **` ` { countryData [ 'todayCases' ] } ` \ n "  +
                f "** { bayrak }  { LANG [ 'TR_CASE' ] } **` ` { countryData [ 'aktif' ] } ` \ n "  +
                f "** { bayrak }  { LANG [ 'TR_ALL_DEATHS' ] } **` ` { countryData [ 'ölümler' ] } ` \ n "  +
                f "** { bayrak }  { LANG [ 'TR_DEATHS' ] } **` ` { countryData [ 'todayDeaths' ] } ` \ n "  +
                f "** { bayrak }  { LANG [ 'TR_HEAL' ] } **` ` { countryData [ 'kurtarıldı' ] } ` \ n "  +
                f "** { bayrak } Test Sayısı: **` ` { countryData [ 'totalTests' ] } ` "
                )
     olayı bekliyoruz . düzenle ( sonuclar )

CmdHelp ( 'covid19' ). add_command (
    'covid' , '<ülke kodu>' , 'Hem Dünya geneli hem de verdiğiniz ülke için güncel Covid 19 keşfedilecek. Ülkeniz farklı ise komutun yanına eklemeniz yeterlidir. ' ,
    'covid az -> Azerbaycanı getirir.'
). add_warning ( '`Ülke yazmazsanız Türkiyeyi seçer.` ). ekle ()