# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# SakirUserBot - SakirBey - Midy

# ██████ DİL SABİTLERİ ██████ #

dan  userbot . dil  içe aktarma  get_value
LANG  =  get_value ( "sohbet" )

# ████████████████████████████████ #

"" "Userid, chatid ve log komutlarını içeren UserBot modülü" ""

dan  asyncio  ithalat  uyku
dan  userbot  ithalat  CMD_HELP , BOTLOG , BOTLOG_CHATID , bot
dan  userbot . olayları  içe aktarma  kaydı
dan  userbot . modüller . admin  get_user_from_event içe  aktarımı
dan  userbot . ana  ithalat  PLUGIN_MESAJLAR

@ register ( giden = Doğru , desen = "^ .id $" )
async  def  useridgetter ( hedef ):
    ID komut verir "" ".userid komutu
    message  =  hedef bekliyor  . get_reply_message ()
    eğer  mesaj :
        eğer  değil  mesajı . ileri :
            user_id  =  mesaj . gönderen . İD
            eğer  mesaj . gönderen . kullanıcı adı :
                isim  =  "@"  +  mesaj . gönderen . Kullanıcı adı
            başka :
                name  =  "**"  +  mesaj . gönderen . first_name  +  "**"
        başka :
            user_id  =  mesaj . ileri . gönderen . İD
            eğer  mesaj . ileri . gönderen . kullanıcı adı :
                isim  =  "@"  +  mesaj . ileri . gönderen . Kullanıcı adı
            başka :
                isim  =  "*"  +  mesaj . ileri . gönderen . first_name  +  "*"
         hedef bekliyor . edit ( "** {} ** {} \ n ** {} **` {} `" . format (
            DİL [ 'ADI' ], adı , DİL [ 'Kimlik' ], user_id ))


@ kayıt ( giden = Doğru , desen = "^ .link (?: | $) (. *)" )
zaman uyumsuz  def  kalıcı bağlantı ( bahsedin ):
    "" ".link yürütme profili kapsama metin ile yapılabilir hale getirir" ""
    Kullanıcı , özel  =  bekliyoruz  get_user_from_event ( söz )
    eğer  değil  kullanıcı :
        dönüş
    eğer  özel :
         bahsetmeyi bekliyorum . düzenle ( f "[ { özel } ] (tg: // kullanıcı? id = { kullanıcı . id } )" )
    başka :
        tag  =  kullanıcı . first_name . değiştir ( " \ u2060 " ,
                                      "" ) eğer  kullanıcı . first_name  else  kullanıcı . Kullanıcı adı
         bahsetmeyi bekliyorum . düzenle ( f "[ { tag } ] (tg: // kullanıcı? id = { kullanıcı . id } )" )


@ kayıt ( giden = Doğru , desen = "^ .chatid $" )
eşzamansız  def  chatidgetter ( sohbet ):
    "" ".chatid komutu çalıştırılır grubun ID komut verir" ""
     sohbeti bekleyin . düzenle ( f " { LANG [ 'GRUP' ] } " "  +  str ( chat . chat_id ) +  " `" )


@ register ( giden = Doğru , desen = r "^. günlük (?: | $) ([\ s \ S] *)" )
eşzamansız  def  günlüğü ( günlük_metni ):
    "" ".log komutu mesajı gönderildiği kadar" ""
    eğer  BOTLOG :
        eğer  log_text . response_to_msg_id :
            reply_msg  =  bekliyoruz  log_text . get_reply_message ()
            bekliyoruz  reply_msg . forward_to ( BOTLOG_CHATID )
        elif  log_text . pattern_match . grup ( 1 ):
            user  =  f "#LOG / Grup Kimliği: { log_text . chat_id } \ n \ n "
            textx  =  kullanıcı  +  log_text . pattern_match . grup ( 1 )
             botu bekleyin . gönder_mesaj ( BOTLOG_CHATID , metinx )
        başka :
            bekliyoruz  log_text . edit ( "Bununla birlikte ne yapmam gerekiyor?" )
            dönüş
        bekliyoruz  log_text . edit ( "` Günlüğe Kaydedildi` " )
    başka :
        bekliyoruz  log_text . düzenle ( LANG [ 'NEED_LOG' ])
     uykuyu bekle ( 2 )
    bekliyoruz  log_text . sil ()


@ register ( giden = Doğru , desen = "^ .kickme? (. *)" )
eşzamansız  def  kickme ( bırakın ):
    "" ".kickme komutu gruptan çıkmaya yarar" ""
    sebep  =  izin . pattern_match . grup ( 1 )
    sohbet  =  ayrılmayı bekle  . get_chat ()
    eğer  Sebep :
         ayrılmayı bekleyin . düzenle ( f " { PLUGIN_MESAJLAR [ 'kickme' ] } \ n ** Sebep: **` { sebep } "" . format (
            id = sohbet . id ,
            başlık = sohbet . başlık ,
            member_count = Sohbet ediyorsanız "  Bilinmiyor " . participants_count == Yok başka ( sohbet . participants_count - 1 )      
        ))
    başka :
         ayrılmayı bekleyin . düzenle ( f " { PLUGIN_MESAJLAR [ 'kickme' ] } " . format (
            id = sohbet . id ,
            başlık = sohbet . başlık ,
            member_count = Sohbet ediyorsanız "  Bilinmiyor " . participants_count == Yok başka ( sohbet . participants_count - 1 )      
        ))
     ayrılmayı bekleyin . müşteri . kick_participant ( ayrılın . chat_id , 'ben' )


@ register ( outgoing = True , pattern = "^ .unmutechat $" )
eşzamansız  def  unmute_chat ( unm_e ):
    "" ".unmutechat komutu susturulmuş grubun sesini açar" ""
    dene :
        dan  userbot . modüller . sql_helper . keep_read_sql okunmamış  içe  aktar
     AttributeError dışında :
        bekliyorum  unm_e . edit ( '"SQL dışı modda çalışıyor!' ' )
        dönüş
    okunmamış ( str ( unm_e . chat_id ))
    bekliyorum  unm_e . düzenle ( LANG [ 'UNMUTED' ])
     uykuyu bekle ( 2 )
    bekliyorum  unm_e . sil ()


@ register ( outgoing = True , pattern = "^ .mutechat $" )
async  def  mute_chat ( mute_e ):
    "" ".mutechat komutu grubu susturur" ""
    dene :
        dan  userbot . modüller . sql_helper . keep_read_sql kread  içe  aktar
     AttributeError dışında :
        bekliyoruz  mute_e . edit ( "` SQL dışı modda çalışıyor! `" )
        dönüş
    bekliyoruz  mute_e . düzenle ( str ( mute_e . chat_id ))
    kread ( str ( mute_e . chat_id ))
    bekliyoruz  mute_e . düzenle ( DİL [ 'SESSİZ' ])
     uykuyu bekle ( 2 )
    bekliyoruz  mute_e . sil ()
    eğer  BOTLOG :
        bekliyoruz  mute_e . müşteri . mesaj_gönder (
            BOTLOG_CHATID ,
            str ( mute_e . chat_id ) +  "susturuldu." )


@ register ( incoming = True , disable_errors = True )
eşzamansız  def  keep_read ( mesaj ):
    "" "Sessiz mantığı." ""
    dene :
        dan  userbot . modüller . sql_helper . keep_read_sql  içe aktarma  is_kread
     AttributeError dışında :
        dönüş
    kread  =  is_kread ()
    eğer  kread :
        için  i  de  kread :
            eğer  ben . groupid  ==  str ( mesaj . chat_id ):
                 mesajı bekleyin . müşteri . send_read_acknowledge ( mesaj . chat_id )


# Regex-Ninja modülü için teşekkürler @Sakirhackofficial999
regexNinja  =  Yanlış


@ register ( giden = Doğru , desen = "^ s /" )
async  def  sedNinja ( etkinlik ):
    "" "Regex-ninja modülü için, s / ile arama otomatik silme komutu" ""
    eğer  regexNinja :
        bekliyoruz  uyku ( .5 )
         olayı bekliyoruz . sil ()


@ register ( outgoing = True , pattern = "^ .regexninja (on | off) $" )
async  def  sedNinjaToggle ( olay ):
    "" "Regex ninja modülünü etkinleştirir veya devre dışı onaylı." ""
    genel  regex
    eğer  olay . pattern_match . grup ( 1 ) ==  "açık" :
        regexNinja  =  Doğru
         olayı bekliyoruz . edit ( "" Regexbot için ninja modu etkinleştirdi.` " )
         uykuyu bekle ( 1 )
         olayı bekliyoruz . sil ()
    elif  olayı . pattern_match . grup ( 1 ) ==  "kapalı" :
        regexNinja  =  Yanlış
         olayı bekliyoruz . edit ( "` `Regexbot için ninja modu devre dışı bırakıldı.` )
         uykuyu bekle ( 1 )
         olayı bekliyoruz . sil ()


CMD_HELP . güncelleme ({
    "sohbet" :
    ".chatid \
\ n Kullanım: Belirlenen grubun ID verir \
\ n \ n .id \
\ n Kullanım: Belirlenen süresi ID verir. \
\ n \ n .log \
\ n Kullanım: Yanıtlanan mesajı gönder. \
\ n \ n .kickme \
\ n Kullanım: Belirlenen gruptan ayrılmanızı sağlar. \
\ n \ n .unmutechat \
\ n Kullanım: Susturulmuş bir sohbetin sesini açar. \
\ n \ n .mutechat \
\ n Kullanım: Belirlenen grubu susturur. \
\ n \ n .link <kullanıcı adı / kullanıcı id>: <isteğe bağlı metin> (veya) herhangi birinin mesajına .link ile yanıt verin <isteğe bağlı metin> \
\ n Kullanım: Kullanılabilir özel metin ile dosya profiline kalıcı bir bağlantı oluşturma. \
\ n \ n .regexninja açık / kapalı \
\ n Kullanım: Küresel olarak regex ninja modülünü etkinleştirir / devre dışı bırakın. \
\ n Regex ninja modülü regex bot tarfından tetiklenen mesajları silmek için yardımcı olur. "
})