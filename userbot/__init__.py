# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# Satır içi bot desteği oluşturduğunuz için teşekkürler github.com/spechide.
# SakirUserBot - Sakir By - Midy - Berceste
"" "UserBot hazırlanışı." ""


dan  Yardım kampanyasının . olaylar  ithalat  callbackquery , InlineQuery , newMessage
dan  Yardım kampanyasının . tl . fonksiyonlar . kanallar  JoinChannelRequest'i içe  aktarır
dan  günlüğü  ithalat  basicConfig , getLogger , INFO , DEBUG
dan  Yardım kampanyasının . senkronize  içe aktar  TelegramClient , özel
dan  Yardım kampanyasının . oturumlar  StringSession'ı içe  aktarır
dan  distutils . util  ithalat  strtobool  olarak  sb
dan  pylast  ithalat  LastFMNetwork , md5
dan  dotenv  ithalat  load_dotenv
dan  pySmartDL  ithalat  SmartDL
dan  sys  ithalat  version_info
dan  Sqlite3  ithalat  bağlamak
dan  istekler  ithalat  get
dan  yeniden  ithalat  derleme
dan  matematik  ithalat  ceil
ithalat  zamanı
 işletim sistemini içe aktar

load_dotenv ( "config.env" )

# Günlükleri kurulumu:
CONSOLE_LOGGER_VERBOSE  =  sb ( os . Envirom . Get ( "CONSOLE_LOGGER_VERBOSE" , "False" ))

ASYNC_POOL  = []

eğer  CONSOLE_LOGGER_VERBOSE :
    basicConfig (
        format = "% (asctime) s - @Sakirhackofficial999 -% (levelname) s -% (message) s" ,
        düzey = HATA AYIKLAMA ,
    )
başka :
    basicConfig ( format = "% (asctime) s - @Sakirhackofficial999 -% (levelname) s -% (message) s" ,
                düzey = BİLGİ )
TOMRUĞU  =  getLogger ( __name__ )

Eğer  version_info [ 0 ] <  3  veya  version_info [ 1 ] <  6 :
    KAYITLAR . bilgi ( "En az python 3.6 sürümüne sahip olmanız gerekir."
              "Birden fazla özellik bağlıdır. Bot kapatılıyor." )
    çık ( 1 )

# Yapılandırmanın önceden kullanılacak değişkeni kullanılarak düzenlenip düzenlenmemiş kontrol edin.
# Temel olarak, yapılandırma dosyası kontrol.
CONFIG_CHECK  =  os . envirom . olsun (
    "___________LUTFEN_______BU_____SATIRI_____SILIN__________" , Yok )

eğer  CONFIG_CHECK :
    KAYITLAR . bilgi (
        "Lütfen ilk hashtag'de belirtilen bilgisi config.env dosyasından"
    )
    çık ( 1 )

    
# SakirUserBot versiyon
SAKIR_VERSION  =  " v1.9.5 "

# Telegram API KEY ve HASH
API_KEY  =  os . envirom . get ( "API_KEY" , Yok )
API_HASH  =  os . envirom . get ( "API_HASH" , Yok )

SILINEN_PLUGIN  = {}

# UserBot Oturum Dizesi
STRING_SESSION  =  os . envirom . get ( "STRING_SESSION" , Hiçbiri )

# Kanal / Grup ID yapılandırmasını günlüğe kaydetme.
BOTLOG_CHATID  =  int ( os . Envirom . Get ( "BOTLOG_CHATID" , Yok ))

# Bot'un dili
DİL  =  os . envirom . get ( "DİL" , "VARSAYILAN" ). üst ()

eğer  değil  DİL  içinde [ "EN" , "TR" , "AZ" , "UZ" , "DEFAULT" ]:
    KAYITLAR . info ( "Bilinmeyen bir dil yazdınız. Bundan dolayı DEFAULT kullanılıyor." )
    DİL  =  "VARSAYILAN"


# UserBot günlükleme özelliği.
BOTLOG  =  sb ( os . Envirom . Get ( "BOTLOG" , "False" ))
LOGSPAMMER  =  sb ( os . Envirom . Get ( "LOGSPAMMER" , "False" ))

# Hey! Bu bir bot. Endişelenme;)
PM_AUTO_BAN  =  sb ( os . Envirom . Get ( "PM_AUTO_BAN" , "False" ))

# Ayrıntılı konsol günlügü
CONSOLE_LOGGER_VERBOSE  =  sb ( os . Envirom . Get ( "CONSOLE_LOGGER_VERBOSE" , "False" ))

# Afk mesajların iletilmesi

AFKILETME  =  sb ( os . Envirom . Get ( "AFKILETME" , "Doğru" ))

# SQL Veritabanı
DB_URI  =  os . envirom . get ( "DATABASE_URL" , "sqlite: ///sakir.db" )

# OCR API anahtarı
OCR_SPACE_API_KEY  =  os . envirom . get ( "OCR_SPACE_API_KEY" , Yok )

# remove.bg API anahtarı
REM_BG_API_KEY  =  işletim sistemi . envirom . get ( "REM_BG_API_KEY" , Yok )

# OTOMATİK PP
AUTO_PP  =  os . envirom . get ( "AUTO_PP" , Yok )

# Modül uyar
WARN_LIMIT  =  int ( os . Envirom . Get ( "WARN_LIMIT" , 3 ))
WARN_MODE  =  os . envirom . get ( "WARN_MODE" , "gmute" )

eğer  değil  WARN_MODE  içinde [ "gmute" , "gban" ]:
    WARN_MODE  =  "gmute"

# Galeri
GALERI_SURE  =  int ( os . Envirom . Get ( "GALERI_SURE" , 60 ))

# Chrome sürücüsü ve Google Chrome dosyaları
CHROME_DRIVER  =  os . envirom . get ( "CHROME_DRIVER" , Yok )
GOOGLE_CHROME_BIN  =  os . envirom . get ( "GOOGLE_CHROME_BIN" , Yok )

#Zaman
ÇALIŞMA ZAMANI  =  zaman . zaman ()

PLUGINID  =  os . envirom . get ( "PLUGIN_CHANNEL_ID" , Yok )
# Eklenti İçin
eğer  değil  PLUGINID :
    PLUGIN_CHANNEL_ID  =  "ben"
başka :
    PLUGIN_CHANNEL_ID  =  int ( PLUGINID )

# OpenWeatherMap API Anahtarı
OPEN_WEATHER_MAP_APPID  =  işletim sistemi . envirom . get ( "OPEN_WEATHER_MAP_APPID" , Yok )
WEATHER_DEFCITY  =  os . envirom . get ( "WEATHER_DEFCITY" , Yok )

# Anti Spambot
ANTI_SPAMBOT  =  sb ( os . Envirom . Get ( "ANTI_SPAMBOT" , "False" ))
ANTI_SPAMBOT_SHOUT  =  sb ( os . Envirom . Get ( "ANTI_SPAMBOT_SHOUT" , "Doğru" ))

# Youtube API anahtarı
YOUTUBE_API_KEY  =  os . envirom . get ( "YOUTUBE_API_KEY" , Hiçbiri )

# Saat & Tarih - Ülke ve Saat Dilimi
ÜLKE  =  str ( os . Envirom . Get ( "ÜLKE" , "" ))
TZ_NUMBER  =  int ( os . Envirom . Get ( "TZ_NUMBER" , 1 ))

# Sevgili :)
SEVGILI  =  os . envirom . get ( "SEVGILI" , Yok )

# Temiz Karşılama
CLEAN_WELCOME  =  sb ( os . Envirom . Get ( "CLEAN_WELCOME" , "Doğru" ))

# Last.fm Modülü
BIO_PREFIX  =  os . envirom . get ( "BIO_PREFIX" , "@Sakirhackofficial999 |" )
DEFAULT_BIO  =  os . envirom . get ( "DEFAULT_BIO" , Yok )

LASTFM_API  =  os . envirom . get ( "LASTFM_API" , Yok )
LASTFM_SECRET  =  os . envirom . get ( "LASTFM_SECRET" , Hiçbiri )
LASTFM_USERNAME  =  os . envirom . get ( "LASTFM_USERNAME" , Hiçbiri )
LASTFM_PASSWORD_PLAIN  =  işletim sistemi . envirom . get ( "LASTFM_PASSWORD" , Hiçbiri )
LASTFM_PASS  =  md5 ( LASTFM_PASSWORD_PLAIN )
eğer  LASTFM_API  ve  LASTFM_SECRET  ve  LASTFM_USERNAME  ve  LASTFM_PASS :
    lastfm  =  LastFMNetwork ( api_key = LASTFM_API ,
                           api_secret = LASTFM_SECRET ,
                           kullanıcı adı = LASTFM_USERNAME ,
                           password_hash = LASTFM_PASS )
başka :
    lastfm  =  Yok

# Google Drive Modülü
G_DRIVE_CLIENT_ID  =  os . envirom . get ( "G_DRIVE_CLIENT_ID" , Yok )
G_DRIVE_CLIENT_SECRET  =  os . envirom . get ( "G_DRIVE_CLIENT_SECRET" , Yok )
G_DRIVE_AUTH_TOKEN_DATA  =  os . envirom . get ( "G_DRIVE_AUTH_TOKEN_DATA" , Yok )
GDRIVE_FOLDER_ID  =  os . envirom . get ( "GDRIVE_FOLDER_ID" , Yok )
TEMP_DOWNLOAD_DIRECTORY  =  işletim sistemi . envirom . get ( "TMP_DOWNLOAD_DIRECTORY" ,
                                         "./downloads" )

# Revert yani Klondan Sonra hesabın eski haline dönmesi
DEFAULT_NAME  =  os . envirom . get ( "DEFAULT_NAME" , Hiçbiri )

# Inline yardımın çalışması için
BOT_TOKEN  =  işletim sistemi . envirom . get ( "BOT_TOKEN" , Yok )
BOT_USERNAME  =  işletim sistemi . envirom . get ( "BOT_USERNAME" , Yok )

# Genius modülünün çalışması için bundan değeri alın https://genius.com/developers ikisi de aynı değerlere sahiptir
GENIUS  =  os . envirom . get ( "GENIUS" , Hiçbiri )

PM_AUTO_BAN_LIMIT  =  int ( os . Envirom . Get ( "PM_AUTO_BAN_LIMIT" , 4 ))

SPOTIFY_DC  =  işletim sistemi . envirom . get ( "SPOTIFY_DC" , Yok )
SPOTIFY_KEY  =  işletim sistemi . envirom . get ( "SPOTIFY_KEY" , Hiçbiri )

PAKET_ISMI  =  os . envirom . get ( "PAKET_ISMI" , "🌃 @Sakirhackofficial999 Paketi" )

# Userbotu oluşturmak için gruplar
BLACKLIST_CHAT  =  os . envirom . get ( "BLACKLIST_CHAT" , Yok )

eğer  değil  BLACKLIST_CHAT : # Eger ayarlanmamışsa Siri Destek grubu Eklenir.
    BLACKLIST_CHAT  = [ - 1001457702125 , - 1001168760410 ]

# Otomatik Katılma ve güncellemeler
OTOMATIK_KATILMA  =  sb ( os . Envirom . Get ( "OTOMATIK_KATILMA" , "Doğru" ))
AUTO_UPDATE  =   sb ( os . Envirom . Get ( "AUTO_UPDATE" , "Doğru" ))

CMD_HELP  = {}
CMD_HELP_BOT  = {}

# Özel Pattern'ler
DESENLER  =  os . envirom . get ( "DESENLER" , ".;!," )
BEYAZ LİSTESİ  =  get ( 'http://gitlab.com/ErdemBey1/siri/-/raw/master/whitelist.json' ). json ()


# Güncelleyici için Heroku hesap bilgileri.
HEROKU_MEMEZ  =  sb ( os . Envirom . Get ( "HEROKU_MEMEZ" , "False" ))
HEROKU_APPNAME  =  işletim sistemi . envirom . get ( "HEROKU_APPNAME" , Hiçbiri )
HEROKU_APIKEY  =  os . envirom . get ( "HEROKU_APIKEY" , Hiçbiri )

# Güncelleyici için repo linki.
UPSTREAM_REPO_URL  =  işletim sistemi . envirom . olsun (
    "UPSTREAM_REPO_URL" ,
    "https://github.com/erdembey1/SiriUserBot.git" )

# Bot versiyon kontrolü
forceVer  = []
eğer  os . yol . var ( "force-surum.check" ):
    os . kaldır ( "force-surum.check" )
başka :
    KAYITLAR . bilgi ( "Force Sürüm Kontrol dosyası yok, getiriliyor ..." )

URL  =  'https://gitlab.com/must4f/VaveylaData/-/raw/main/force-surum.check' 
ile  açık ( 'kuvvet-surum.check' , 'wb' ) olarak  yük :
    yük . yaz ( get ( URL ). içerik )
    
DB  =  bağlan ( "force-surum.check" )
CURSOR  =  DB . imleç ()
CURSOR . execute ( "" "SURUM1'DEN SEÇ *" "" )
ALL_ROWS  =  CURSOR . fetchall ()

 ALL_ROWS içindeki i  için : 
    forceVer . ekle ( i [ 0 ])
bağlan ( "force-surum.check" ). kapat ()

# CloudMail.ru ve MEGA.nz ayarlama
eğer  değil  os . yol . var ( 'bin' ):
    os . mkdir ( 'bin' )

ikili dosyalar  = {
    "https://raw.githubusercontent.com/erdembey1/megadown/master/megadown" :
    "bin / megadown" ,
    "https://raw.githubusercontent.com/erdembey1/cmrudl.py/master/cmrudl.py" :
    "bin / cmrudl"
}

için  ikili , yolu  içinde  ikili . öğeler ():
    downloader  =  SmartDL ( ikili , yol , progress_bar = Yanlış )
    downloader . başlangıç ()
    os . chmod ( yol , 0o755 )

# 'bot' değişkeni
eğer  STRING_SESSION :
    # pylint: devre dışı = geçersiz ad
    bot  =  TelegramClient ( StringSession ( STRING_SESSION ), API_KEY , API_HASH )
başka :
    # pylint: devre dışı = geçersiz ad
    bot  =  TelegramClient ( "userbot" , API_KEY , API_HASH )

ASISTAN  =  1758581185  # Bot yardımcısı
ASISTANUSERNAME  =  'muinrobot'

eğer  os . yol . var ( "öğrenme-verisi-root.check" ):
    os . remove ( "learning-data-root.check" )
başka :
    KAYITLAR . info ( "Braincheck dosyası yok, getiriliyor ..." )

URL  =  'https://gitlab.com/must4f/VaveylaData/-/raw/main/learning-data-root.check'
ile  açık ( 'öğrenme-veri root.check' , 'wb' ) olarak  yük :
    yük . yaz ( get ( URL ). içerik )

eşzamansız  def  check_botlog_chatid ():
    eğer  değil  BOTLOG_CHATID  ve  LOGSPAMMER :
        KAYITLAR . bilgi (
            "Özel hata günlüğünün çalışması için yapılandırmadan BOTLOG_CHATID değişkenini ayarlamanız gerekir." )
        çık ( 1 )

    elif  değil  BOTLOG_CHATID  ve  BOTLOG :
        KAYITLAR . info ( "Günlüğe kaydetme özelliğinin çalışması için yapılandırmadan BOTLOG_CHATID değişkenini ayarlamanız gerekir." )
        çık ( 1 )

    elif  BOTLOG değil  veya LOGSPAMMER değil :   
        dönüş

    varlık  =  bekleme  botu . get_entity ( BOTLOG_CHATID )
    eğer  varlık . default_banned_rights . mesaj_gönder :
        KAYITLAR . bilgi (
            "Hesabınızın BOTLOG_CHATID grubuna mesaj gönderme yetkisi yoktur."
            "Grup ID'sini doğru yazıp yazmadığınızı kontrol edin." )
        çık ( 1 )
        
eğer  değil  BOT_TOKEN  ==  Yok :
    tgbot  =  TelegramClient (
        "TG_BOT_TOKEN" ,
        api_id = API_KEY ,
        api_hash = API_HASH
    ). start ( bot_token = BOT_TOKEN )
başka :
    tgbot  =  Yok

def  butonlastir ( sayfa , modül ):
    Satir  =  5
    Kolon  =  2
    
    Modüller  =  sıralanmış ([ modül  için  modülüne  de  Modüller  halinde  değil  modül . startswith ( "_" )])
    çiftler  =  liste ( harita ( liste , zip ( moduller [:: 2 ], moduller [ 1 :: 2 ])))
    Eğer  Len ( Modüller ) %  2  ==  1 :
        çiftler . Ekleme ([ Modüller [ - 1 ]])
    max_pages  =  ceil ( len ( çiftler ) /  Satir )
    çiftleri  = [ çiftleri [ i : i  +  Satır ] için  i  içinde  aralığı ( 0 , len ( çiftleri ), Satır )]
    butonlar  = []
    için  çift  olarak  çiftler [ sayfa ]:
        düğmeler . ekle ([
            özel . Düğme . içi ( "✨"  +  çift , veri = f "bilgi [ { sayfa } ] ( { çifti } )" ) için  çift  olarak  çiftler
        ])

    butonlar . append ([ custom . Button . inline ( "◀ ️ Geri" , data = f "sayfa ( { ( max_pages  -  1 ) if  sayfa  ==  0  else ( sayfa  -  1 ) } )" ), custom . Button . inline ( "İleri ▶ ️ " , data = f" sayfa ( { 0  if  sayfa  == ( max_pages -  1 ) else  sayfa  +  1 } ) ")])
    Dönüş : [ MAX_PAGES , butonlar ]

ile  bot : # ü
    dene :
        bot ( JoinChannelRequest ( "@sakirhackofficial999" ))
        bot ( JoinChannelRequest ( "@Sakirhackofficial999" ))
        eğer  OTOMATIK_KATILMA :
            bot ( JoinChannelRequest ( "@Sakirhackofficial999" ))
        başka :
            geçmek
    hariç :
        geçmek

    moduller  =  CMD_HELP
    ben  =  bot . get_me ()
    uid  =  ben . İD

    dene :
        @ tgbot . on ( NewMessage ( pattern = '/ start' ))
        eşzamansız  def  start_bot_handler ( olay ):
            eğer  değil  olay . mesaj . from_id  ==  uid :
                 olayı bekliyoruz . yanıt ( f'`Merhaba ben` @Sakirhackofficial999`! Ben sahibime (`@ { me . username } `) yardımcı olmak için varım, yaani sana yardımcı olamam: / Ama sen de bir Siri'ye izin verensin; Kanala bak` @Sakirfed ' )
            başka :
                 olayı bekliyoruz . cevap ( f''Tengri Türkleri kurtar! Siri çalışıyor ... '' )

        @ tgbot . on ( InlineQuery )   # pylint: devre dışı bırak = E0602
        async  def  inline_handler ( olay ):
            oluşturucu  =  olay . inşaatçı
            sonuç  =  Yok
            sorgu  =  olay . Metin
            eğer  olay . sorgu . user_id  ==  uid  ve  query  ==  "@Sakirhackofficial999" :
                rev_text  =  sorgu [:: - 1 ]
                veriler  = ( butonlastir ( 0 , sıralı ( CMD_HELP )))
                sonuç  =  oluşturucu bekleyin  . makale (
                    f "Lütfen Sadece .yardım Komutu İle Kullanın" ,
                    text = f "** En Gelişmiş UserBot! ** [Sakir] (https://t.me/sakirfed) __Çalışıyor ...__ \ n \ n ** Yüklenen Modül Sayısı: **` { len ( CMD_HELP ) } ` \ n ** Sayfa: ** 1 / { veriler [ 0 ] } " ,
                    düğmeler = veriler [ 1 ],
                    link_preview = Yanlış
                )
            elif  sorgusu . startswith ( "http" ):
                parca  =  sorgu . bölme ( "" )
                sonuç  =  oluşturucu . makale (
                    "Dosya Yüklendi" ,
                    text = f "** Dosya başarılı bir şekilde { parca [ 2 ] } uygulama yüklendi! ** \ n \ n Yükleme zamanı: { parca [ 1 ] [: 3 ] } saniye \ n [] ( { parca [ 0 ] } ) " ,
                    düğmeler = [
                        [ özel . Düğme . url ( 'URL' , parca [ 0 ])]
                    ],
                    link_preview = Doğru
                )
            başka :
                sonuç  =  oluşturucu . makale (
                    "@Sakirhackofficial999" ,
                    text = "" "🎉 @ SakirUserBot'u satın almayı deneyin!
Hesabınızı bot'a çevirebilirsiniz ve bunları kullanabilirsiniz. Unutmayın, siz başkasının botunu yönetemezsiniz! Alttaki GitHub'ın tüm kurulum detayları anlatılmıştır. "" " ,
                    düğmeler = [
                        [ özel . Düğme . url ( "Kanala Katıl" , "https://t.me/SakirUserBot" ), özel . Düğme . url (
                            "Gruba Katıl" , "https://t.me/sakirfed" )],
                        [ özel . Düğme . url (
                            "GitHub" , "" )]
                    ],
                    link_preview = Yanlış
                )
             olayı bekliyoruz . Cevap ([ sonuç ] eğer  sonuç  başka  Yok )

        @ tgbot . on ( callbackquery . CallbackQuery ( data = compile ( b "sayfa \ ((. +?) \)" )))
        async  def  sayfa ( olay ):
            eğer  değil  olay . sorgu . user_id  ==  uid :
                dönüş  bekleme  olayı . answer ( "❌ Hey! Benim mesajlarımı düzenlemeye kalkma! Kendine bir @SakirUserBot kur." , cache_time = 0 , alert = True )
            sayfa  =  int ( event . data_match . group ( 1 ). decode ( "UTF-8" ))
            veriler  =  butonlastir ( sayfa , CMD_HELP )
             olayı bekliyoruz . düzenle (
                f "** En Gelişmiş UserBot! ** [Sakir] (https://t.me/SakirUserBot) __Çalışıyor ...__ \ n \ n ** Yüklenen Modül Sayısı: **` { len ( CMD_HELP ) } ` \ n ** Sayfa: ** { sayfa  +  1 } / { veriler [ 0 ] } " ,
                düğmeler = veriler [ 1 ],
                link_preview = Yanlış
            )
        
        @ tgbot . on ( geri arama sorgusu . CallbackQuery ( veri = derleme ( b "bilgi \ [(\ d *) \] \ ((. *) \)" )))
        async  def  bilgi ( olay ):
            eğer  değil  olay . sorgu . user_id  ==  uid :
                dönüş  bekleme  olayı . answer ( "❌ Hey! Benim mesajlarımı düzenlemeye kalkma! Kendine bir @SakirUserBot kur." , cache_time = 0 , alert = True )

            sayfa  =  int ( event . data_match . group ( 1 ). decode ( "UTF-8" ))
            komut  =  olay . data_match . grup ( 2 ). decode ( "UTF-8" )
            dene :
                butonlar  = [ özel . Düğme . CMD_HELP_BOT [ komut ] [ 'komutlar' ] içinde cmd için satır içi ( "🔹"  +  cmd [ 0 ], data = f "komut [ { komut } [ { sayfa } ]] ( { cmd [ 0 ] } )" ) . öğeler ()]   
             KeyError hariç :
                dönüş  bekleme  olayı . answer ( "❌ Bu modüle açıklama yazılmamış." , cache_time = 0 , alert = True )

            butonlar  = [ butonlar [ i : i  +  2 ] için  i  içinde  aralığı ( 0 , len ( butonlar ), 2 )]
            düğmeler . Ekleme ([ özel . Düğme . inline ( "◀ ️ Geri" , veriler = f "sayfa ( { sayfa } )" )])
             olayı bekliyoruz . düzenle (
                f "** 📗 Dosya: **` { komut } ` \ n ** 🔢 Komut Sayısı: **` { len ( CMD_HELP_BOT [ komut ] [ 'komutlar' ]) } `" ,
                düğmeler = butonlar ,
                link_preview = Yanlış
            )
        
        @ tgbot . on ( geri arama sorgusu . CallbackQuery ( veri = derleme ( b "komut \ [(. *) \ [(\ d *) \] \] \ ((. *) \)" )))
        async  def  komut ( olay ):
            eğer  değil  olay . sorgu . user_id  ==  uid :
                dönüş  bekleme  olayı . answer ( "❌ Hey! Benim mesajlarımı düzenlemeye kalkma! Kendine bir @SakirUserBot kur." , cache_time = 0 , alert = True )

            cmd  =  olay . data_match . grup ( 1 ). decode ( "UTF-8" )
            sayfa  =  int ( event . data_match . group ( 2 ). decode ( "UTF-8" ))
            komut  =  olay . data_match . grup ( 3 ). decode ( "UTF-8" )

            sonuç  =  f "✨` { cmd } `** Dosyası: ** \ n "
            eğer  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'bilgi' ] ==  '' :
                eğer  değil  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'uyarı' ] ==  '' :
                    sonuç  + =  f "** ⬇️ Resmi: ** { '✅'  eğer  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'resmi' ] başka  '❌' } \ n "
                    sonuç  + =  f "** ⚠️ Uyarı: ** { CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'uyarı' ] } \ n \ n "
                başka :
                    sonuç  + =  f "** ⬇️ Resmi: ** { '✅'  eğer  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'resmi' ] başka  '❌' } \ n \ n "
            başka :
                sonuç  + =  f "** ⬇️ Resmi: ** { '✅'  eğer  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'resmi' ] başka  '❌' } \ n "
                eğer  değil  CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'uyarı' ] ==  '' :
                    sonuç  + =  f "** ⚠️ Uyarı: ** { CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'uyarı' ] } \ n "
                sonuç  + =  f "** ℹ️ Bilgi: ** { CMD_HELP_BOT [ cmd ] [ 'bilgi' ] [ 'bilgi' ] } \ n \ n "

            command  =  CMD_HELP_BOT [ cmd ] [ 'komutlar' ] [ komut ]
            eğer  komut [ 'parametreler' ] ise  Hiçbiri :
                sonuç  + =  f "** 🛠 Komut: ** ' { KALIPLARI [: 1 ] } { komut [ 'komut' ] } ' \ n "
            başka :
                sonuç  + =  f "** 🛠 Komut: ** ' { KALIPLARI [: 1 ] } { komut [ 'komut' ] }  { komut [ 'parametreler' ] } ' \ n "
                
            Eğer  komut [ 'örnek' ] olan  Yok :
                sonuç  + =  f "** 💬 Açıklama: **" { komut [ 'kullanım' ] } " \ n \ n "
            başka :
                sonuç  + =  f "** 💬 Açıklama: **" { komut [ 'kullanım' ] } " \ n "
                sonuç  + =  f "** ⌨️ Örnek: ** ' { KALIPLARI [: 1 ] } { komut [ 'örnek' ] } ' \ n \ n "

             olayı bekliyoruz . düzenle (
                neden ,
                düğmeler = [ özel . Düğme . satır içi ( "◀ ️ Geri" , data = f "bilgi [ { sayfa } ] ( { cmd } )" )],
                link_preview = Yanlış
            )
     e gibi İstisna  hariç : 
        baskı ( e )
        KAYITLAR . bilgi (
            "Botunuzda inline desteği devre dışı bırakıldı."
            "Etkinleştirmek için bir bot'u tanımlayın ve botunuzda inline ile etkinleştirin."
            "Eğer bunun dışında bir sorun olduğunda bize ulaşın t.me/SakirUserBot."
        )

    dene :
        bot . döngü . run_until_complete ( check_botlog_chatid ())
    hariç :
        KAYITLAR . bilgi (
            "BOTLOG_CHATID ortam değişkeni bir pasif değildir."
            "Ortam değişkenlerinizi / config.env yazın kontrol edin."
        )
        çık ( 1 )


# Küresel Değişkenler

USERBOT_  =  Doğru
SON_GORULME  =  0
COUNT_MSG  =  0
KULLANICILAR  = {}
MYID  =  uid
ForceVer  =  int ( forceVer [ 0 ])
BRAIN_CHECKER  = []
COUNT_PM  = {}
LASTMSG  = {}
FUP  =  Doğru
ENABLE_KILLME  =  Doğru
ISAFK  =  Yanlış
AFKREASON  =  Yok
ZALG_LIST  = [[
    "̖" ,
    "̗" ,
    "̘" ,
    "̙" ,
    "̜" ,
    "̝" ,
    "̞" ,
    "̟" ,
    "̠" ,
    "̤" ,
    "̥" ,
    "̦" ,
    "̩" ,
    "̪" ,
    "̫" ,
    "̬" ,
    "̭" ,
    "̮" ,
    "̯" ,
    "̰" ,
    "̱" ,
    "̲" ,
    "̳" ,
    "̹" ,
    "̺" ,
    "̻" ,
    "̼" ,
    "ͅ" ,
    "͇" ,
    "͈" ,
    "͉" ,
    "͍" ,
    "͎" ,
    "͓" ,
    "͔" ,
    "͕" ,
    "͖" ,
    "͙" ,
    "͚" ,
    "" ,
],
    [
    "̍" , "̎" , "̄" , "̅" , "̿" , "̑" , "̆" , "̐" , "͒" , "͗" ,
    "͑" , "̇" , "̈" , "̊" , "" , "" , "" , "͊" , "͋" , "͌" ,
    "̃" , "̂" , "̌" , "͐" , "́" , "̋" , "̏" , "̽" , "̉" , "ͣ" ,
    "ͤ" , "ͥ" , "ͦ" , "ͧ" , "ͨ" , "ͩ" , "ͪ" , "ͫ" , "ͬ" , "ͭ" ,
    "ͮ" , "ͯ" , "̾" , "͛" , "͆" , "̚"
],
    [
    "̕" ,
    "̛" ,
    "̀" ,
    "́" ,
    "͘" ,
    "̡" ,
    "̢" ,
    "̧" ,
    "̨" ,
    "̴" ,
    "̵" ,
    "̶" ,
    "͜" ,
    "͝" ,
    "͞" ,
    "͟" ,
    "͠" ,
    "͢" ,
    "̸" ,
    "̷" ,
    "͡" ,
]]