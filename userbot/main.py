# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# Sakir UserBot - SakirBey - Midy

"" "UserBot başlangıç ​​noktası" ""
ithalat  importlib
dan  importlib  ithalat  import_module
dan  Sqlite3  ithalat  bağlamak
 işletim sistemini içe aktar
ithalat  istekleri
dan  Yardım kampanyasının . tl . türler  InputMessagesFilterDocument dosyasını içe  aktarır
dan  Yardım kampanyasının . hataları . rpcerrorlist  import  PhoneNumberInvalidError
dan  Yardım kampanyasının . tl . fonksiyonlar . kanallar  GetMessagesRequest'i içe  aktarır
itibaren . ithalat  BRAIN_CHECKER , GÜNLÜKLERİ , bot , PLUGIN_CHANNEL_ID , CMD_HELP , DİL , SIRI_VERSION , DESENLER
itibaren . ALL_MODULES modüllerini  içe  aktar
 userbot'u içe aktar . modüller . sql_helper . mesaj_sql  olarak  MSJ_SQL
 userbot'u içe aktar . modüller . sql_helper . galeri_sql  ,  GALERI_SQL olarak
dan  pySmartDL  ithalat  SmartDL
dan  Yardım kampanyasının . tl  içe aktarma  işlevleri

dan  rastgele  ithalat  seçimi
 chromedriver_autoinstaller'ı içe aktar
dan  json  ithalat  yükleri , JSONDecodeError
 yeniden ithal
 userbot'u içe aktar . cmdhelp

ALIVE_MSG  = [
    "" Userbotunuz çalışıyor ve sana bişey demek istiyor .. Seni seviyorum` ** {sakirsahip} ** ❤️ " ,
    "✅` `Tabikide Çalışıyorum Bunun İçin Yapıldım Dostum ...` " ,
    "🎆` Endişelenme! Seni yanlız bırakmam.` ** {sakirsahip} **, "userbot çalışıyor.`" ,
    "` ⛈️ Elimden gelenin en çalışma sırasında hazırım`, ** {sakirsahip}: 3 ** " ,
    "✨` Siri sahibinin emirlerine hazır ... '' ,
    "😔` Gitmez insanlar bile gidiyor olmalı ki hayatta yazarak beni kontrol ediyorsun! Ben dynom bitene kadar` ** {sakirsahip} 'i ** `bırakmam!' ' ,
    "" Şuan en gelişmiş userbotun düzenlediği mesajı okuyor olmalısın` ** {sakirsahip} **. " ,
    "` Huh! `** {sakirsahip} **` beni çağırıyor 🍰 yiyordum ee şey en azından yemiş gibi yapıyorum..` " ,
    "` Hokus Pokus 🔮! Ee şey sanırım büyücülükle aram yok amaime` ** {sakirsahip} ** `düşüyor..`" ,
    "Benimi Aramıştın ❓ Ben Buradayım Merak Etme`"
]

DIZCILIK_STR  = [
    "Çıkartmayı dızlıyorum ..." ,
    "Çaldım Gitti Geçmiş Olsun 🤭" ,
    "Yaşasın dızcılık ..." ,
    "Bu çıkartmayı kendi paketime davet ediyorum ..." ,
    "Bunu dızlamam lazım ..." ,
    "Hey bu güzel bir çıkartma ! \ N Hemen dızlıyorum .." ,
    "Çıkartmanı dızlıyorum \ n hahaha." ,
    "Hey şuraya bak. (☉｡☉)! → \ n Ben bunu dızlarken ..." ,
    "Güller kırmızı menekşeler mavi, bu çıkartmayı paketime havalı olacağım ..." ,
    "Çıkartma hapsediliyor ..." ,
    "Bay dızcı bu çıkartmayı dızlıyor ..." ,
    "Bu güzel çıkartma neden benim paketimde de olmasın🤭" ,
]

AFKSTR  = [
    "Şu an acele işim var, daha sonra mesaj atsan olmaz mı? Zaten yine geleceğim." ,
    "Kişi şu anda telefona cevap veremiyor. Sinyal sesinden sonra kendi tarifeniz üzerinden mesajınızı bırakabilirsiniz. Mesaj ücreti 49 kuruştur. \ N` biiiiiiiiiiiiiiiiiiiiiiiiiiiiip` ! " ,
    "Birkaç dakika içinde geleceğim. Fakat gelmezsem ... \ n daha fazla bekle." ,
    "Şu an burada değilim, ama muhtemelen başka bir yerdeyim." ,
    "Güller kırmızı \ n Menekşeler mavi \ n Bana bir mesaj bırak \ n Ve sana döneceğim." ,
    "Bazen hayattaki en iyi şeyler beklemeye değer… \ n Hemen dönerim." ,
    "Hemen dönerim, \ n ama eğer geri dönmezsem, \ n daha sonra dönerim." ,
    "Henüz anlamadıysan, \ n burada değilim." ,
    "Merhaba, uzak mesajıma hoş geldiniz, bugün sizi nasıl görmezden görebilirim?" ,
    "7 deniz ve 7 ülkeden uzaktayım, \ n 7 su ve 7 kıta, \ n 7 dağ ve 7 tepe, \ n 7 ovala ve 7 höyük, \ n 7 havuz ve 7 göl, \ n 7 bahar ve 7 çayır, \ n 7 şehir ve 7 mahalle, \ n 7 blok ve 7 ev ... \ n \ n Mesajların bile bana ulaşamayacağı bir yer! " ,
    "Şu anda klavyeden uzaktayım, ama ekranınızda yeterince yüksek sesle çığlık da, sizi duyabilirim." ,
    "Şu anda ilerliyorum \ n ---->" ,
    "Şu anda ilerliyorum \ n <----" ,
    "Lütfen mesaj mesajı ve beni zaten olduğumdan daha önemli hissettirin." ,
    "Sahibim burada değil, bu yüzden bana yazmayı bırak." ,
    "Burada olsaydım, \ n Sana nerede olduğumu söylerdim. \ N \ n Ama ben değilim, \ n geri döndüğümde bana sor ..." ,
    "Uzaklardayım! \ N Ne zaman dönerim bilmiyorum! \ N Umarım birkaç dakika sonra!" ,
    "Sahibim şuan da müsait değil. Adınızı, numaranızı ve adresinizi verirseniz ona iletibilirm ve böylelikle geri döndüğü zaman." ,
    "Üzgünüm, sahibim burada değil. \ N O gelene kadar benimle konuşabilirsiniz. \ N Sahibim size sonra döner." ,
    "Bahse girerim bir mesaj bekliyordun!" ,
    "Hayat çok kısa, yapacak çok şey var ... \ n Onlardan birini yapıyorum ..." ,
    "Şu an burada değilim .... \ n ama öyleysem ... \ n \ n bu harika olmaz mıydı?" ,
    "Beni hatırladığına sevindim ama şuanda klavye çok uzak" ,
    "Belki İyiyim, Kötüyüm Bilmiyorsun Ama AFK Olduğumu Görebiliyorsun"
]

KICKME_MSG  = [
    "Güle güle ben gidiyorum 👋🏻" ,
    "Sessizce çıkıyorum 🥴" ,
    "Haberin olmadan çıkarsam bir gün benim grupta fakedeceksin .. O yüzden bu mesajı bırakıyorum🚪" ,
    "Hemen burayı terk etmeliyim🤭" ,
    "7 deniz ve 7 ülke, \ n 7 su ve 7 kıta, \ n 7 dağ ve 7 tepe, \ n 7 ovala ve 7 höyük, \ n 7 havuz ve 7 göl, \ n 7 bahar ve 7 çayır, \ n 7 şehir ve 7 mahalle, \ n 7 blok ve 7 ev ... \ n \ n Kısaca bu gruptan uzak bi yere ..! " ,
    Hadi Ben Kaçtım
]


UNAPPROVED_MSG  = ( "" Hey olduğun yerde kal,! 👨‍💻 Ben SakirUserBot. Endişelenme! \ N \ n `"
                  "` `Sahibim sana mesaj atma izni vermedi o yüzden sahibim seni onaylayana kadar bu mesajı alacaksın .. ''
                  "` Lütfen sahibimin aktif olmasını bekleyin, o genellikle PM'leri onaylar. \ N \ n `"
                  "` Bildiğim kadarıyla o kafayı yemiş insanlara PM izni vermiyor.` )

DB  =  connect ( "learning-data-root.check" )
CURSOR  =  DB . imleç ()
CURSOR . execute ( "" "SEÇ * BEYİN1'DEN" "" )
ALL_ROWS  =  CURSOR . fetchall ()
INVALID_PH  =  ' \ n HATA: Girilen telefon numarası geçersiz' \
             ' \ n   Ipucu: Ülke kodunu kullanarak numaranı gir' \
             ' \ n    Telefon numaranızı tekrar kontrol edin'

 ALL_ROWS içindeki i  için : 
    BRAIN_CHECKER . ekle ( i [ 0 ])
connect ( "learning-data-root.check" ). kapat ()
BRAIN_CHECKER  =  BRAIN_CHECKER [ 0 ]

def  extractCommands ( dosya ):
    FileRead  =  open ( dosya , 'r' ). oku ()
    
    eğer  '/'  in  dosyası :
        dosya  =  dosya . bölme ( '/' ) [ - 1 ]

    Desen  =  yeniden . findall ( r "@register \ (. * pattern = (r |) \" (. *) \ ". * \)" , Dosya Okuması )
    Komutlar  = []

    eğer  yeniden . arama ( r'CmdHelp \ (. * \) ' , FileRead ):
        geçmek
    başka :
        dosyaAdi  =  dosya . değiştirin ( '.py' , '' )
        CmdHelp  =  userbot . cmdhelp . CmdHelp ( dosyaAdi , Yanlış )

        # Komutları Alıyoruz #
        için  Komutanlığı  içinde  Modeli :
            Command  =  Komut [ 1 ]
            Eğer  komut  ==  '  ya  len ( Komut ) <=  1 :
                devam et
            Komut  =  yeniden . findall ( "(^. * [a-zA-Z0-9şğüöçı] \ w)" , Komut )
            Eğer ( len ( Komut ) > =  1 ) ve ( değil  Komut [ 0 ] ==  '' ):
                Komut  =  Komut [ 0 ]
                Eğer  Komut [ 0 ] ==  '^' :
                    KomutStr  =  Komut [ 1 :]
                    Eğer  KomutStr [ 0 ] ==  '' :
                        KomutStr  =  KomutStr [ 1 :]
                    Komutlar . ek ( KomutStr )
                başka :
                    eğer  Komut [ 0 ] ==  '^' :
                        KomutStr  =  Komut [ 1 :]
                        Eğer  KomutStr [ 0 ] ==  '' :
                            KomutStr  =  KomutStr [ 1 :]
                        başka :
                            KomutStr  =  Komut
                        Komutlar . ek ( KomutStr )

            # SIRIPY
            Siripy  =  yeniden . arama ( ' \ " \" \ " SIRIPY (. *) \" \ " \" ' , FILEREAD , yeniden . dotall )
            eğer  değil  Siripy  ==  Yok :
                Siripy  =  Siripy . grup ( 0 )
                için  Satir  içinde  Siripy . bölme çizgileri ():
                    eğer ( değil  ' "" "'  in  Satir ) ve ( ':'  in  Satir ):
                        Satir  =  Satir . bölme ( ':' )
                        İsim  =  Satir [ 0 ]
                        Deger  =  Satir [ 1 ] [ 1 :]
                                
                        eğer  Isim  ==  'INFO' :
                            CmdHelp . add_info ( Deger )
                        elif  Isim  ==  'UYARI' :
                            CmdHelp . add_warning ( Deger )
                        başka :
                            CmdHelp . set_file_info ( Isim , Deger )
            için  komut  içinde  Komutlar :
                # eğer yeniden arama ('\ [(\ w *) \]', Komut):
                    # Komut = re.sub ('(? <= \ [.) [A-Za-z0-9 _] * \]', '', Komut) .replace ('[', '')
                CmdHelp . add_command ( Komut , Yok , 'Bu eklenti Dışarıdan yüklenmiştir. HERHANGİ Bir açıklama tanımlanmamıştır.' )
            CmdHelp . ekle ()

dene :
    bot . başlangıç ()
    idim  =  bot . get_me (). İD
    siribl  =  istekler . get ( 'https://gitlab.com/must4f/VaveylaData/-/raw/main/blacklist.json' ). json ()
    eğer  idim  içinde  siribl :
        bot . send_message ( "ben" , f "❌ Sakir yöneticileri sizi bottan yasakladı! Bot kapatılıyor ...` " )
        KAYITLAR . hatası ( "Sakir yöneticileri sizi bottan yasakladı! Bot kapatılıyor ..." )
        bot . bağlantıyı kes ()
    # ChromeDriver'ı Ayarlayalım #
    dene :
        chromedriver_autoinstaller . yükle ()
    hariç :
        geçmek
    
    # Galeri için değerler
    GALERI  = {}

    # FİŞ MESAJLARI AYARLIYORUZ
    PLUGIN_MESAJLAR  = {}
    ORJ_PLUGIN_MESAJLAR  = { "canlı" : f " { str ( seçim ( ALIVE_MSG )) } " , "afk" : f "" { str ( seçim ( AFKSTR )) } "" , "kickme" : f "" { str ( seçim ( KICKME_MSG )) } "" , "pm" : str ( UNAPPROVED_MSG ), "dızcı" :str (choice ( DIZCILIK_STR )), "ban" : "🌀 {bahsed}`, Banlandı !! " , " sessiz " : " 🌀 {söz} `, oturum alındı!" " , " onayla " : " "Merhaba` {anma } `, artık bana mesaj gönderebilirsin!` " , " ret " : " {söz} `, artık bana mesaj gönderemezsin!" , "blok" : "{anma}`, bunu bana mecbur bıraktın! Seni engelledim! `" }


    PLUGIN_MESAJLAR_TURLER  = [ "canlı" , "afk" , "kickme" , "pm" , "dızcı" , "ban" , "sessiz" , "onayla" , "onaylamama" , "engelle" ]
    için  mesaj  içinde  PLUGIN_MESAJLAR_TURLER :
        dmsj  =  MSJ_SQL . getir_mesaj ( mesaj )
        eğer  dmsj  ==  false :
            PLUGIN_MESAJLAR [ mesaj ] =  ORJ_PLUGIN_MESAJLAR [ mesaj ]
        başka :
            eğer  dmsj . startswith ( "MEDYA_" ):
                medya  =  int ( dmsj . split ( "MEDYA_" ) [ 1 ])
                medya  =  bot . get_messages ( PLUGIN_CHANNEL_ID , ids = medya )

                PLUGIN_MESAJLAR [ mesaj ] =  medya
            başka :
                PLUGIN_MESAJLAR [ mesaj ] =  dmsj
    eğer  değil  PLUGIN_CHANNEL_ID  ==  Yok :
        KAYITLAR . bilgi ( "🔄 Eklentiler Yükleniyor .." )
        dene :
            KanalId  =  bot . get_entity ( PLUGIN_CHANNEL_ID )
        hariç :
            KanalId  =  "ben"

        için  eklentisi  de  bot . iter_messages ( KanalId , filtre = InputMessagesFilterDocument ):
            eğer  eklentisi . dosya . ad  ve ( len ( eklenti . dosya . adı . bölme ( '.' )) >  1 ) \
                ve  eklenti . dosya . isim . bölme ( '.' ) [ - 1 ] ==  'py' :
                Bölme  =  eklenti . dosya . isim . bölünmüş ( "." )

                eğer  değil  os . yol . Varlığından ( "./userbot/modules/"  +  eklentisi . Dosya . ad ):
                    dosya  =  bot . download_media ( eklenti , "./userbot/modules/" )
                başka :
                    KAYITLAR . bilgi ( "Bu Eklenti Onsuzda Yüklüdür"  +  eklenti . dosya . adı )
                    extractCommands ( './userbot/modules/'  +  eklenti . Dosya . ad )
                    dosya  =  eklenti . dosya . isim
                    devam et 
                
                dene :
                    spec  =  importlib . yararlanın . spec_from_file_location ( "userbot.modules."  +  Split [ 0 ], dosya )
                    mod  =  importlib . yararlanın . module_from_spec ( özellik )

                    spec . yükleyici . exec_module ( mod )
                 e gibi İstisna  hariç : 
                    KAYITLAR . bilgi ( f "` [×] Yükleme Başarısız! Eklentisi Hatalı !! \ n \ n Hata: { e } `" )

                    dene :
                        eklenti . sil ()
                    hariç :
                        geçmek

                    eğer  os . yol . Varlığından ( "./userbot/modules/"  +  eklentisi . Dosya . ad ):
                        os . remove ( "./userbot/modules/"  +  eklenti . dosya . adı )
                    devam et
                extractCommands ( './userbot/modules/'  +  eklenti . Dosya . ad )
    başka :
        bot . send_message ( "me" , f "Lütfen eklentinin kalıcı olması için PLUGIN_CHANNEL_ID'i toplu.`" )
 PhoneNumberInvalidError dışında :
    baskı ( INVALID_PH )
    çıkış ( 1 )

async  def  FotoDegistir ( fotoğraf ):
    FOTOURL  =  GALERI_SQL . TUM_GALERI [ fotoğraf ]. foto
    r  =  istekler . olsun ( FOTOURL )

    ile  açık ( str ( foto ) +  ".jpg" , 'wb' ) olarak  f :
        f . Yazma ( r . İçerik )    
    dosya  =  bekleme  botu . upload_file ( str ( fotoğraf ) +  ".jpg" )
    dene :
        bekliyoruz  bot ( fonksiyonlar . fotoğraflar . UploadProfilePhotoRequest (
            dosya
        ))
        dönüş  Doğru
    hariç :
        dönüş  Yanlış

için  MODULE_NAME  içinde  ALL_MODULES :
    imported_module  =  import_module ( "userbot.modules."  +  modül_ismi )

os . sistem ( "temizle" )

KAYITLAR . bilgi ( "+ ============================================ ============= + " )
KAYITLAR . bilgi ( "| ✨Sakir Userbot✨ |" )
KAYITLAR . bilgi ( "+ ============== + ============== + ============== + = ============= + " )
KAYITLAR . bilgi ( "| |" )
KAYITLAR . info ( "Botunuz çalışıyor! Herhangi bir sohbete .alive yazarak Test edin."
          "Yardıma İhtiyacınız varsa, Destek grubumuza gelin t.me/SakirUserBot" )
KAYITLAR . bilgi ( f "Bot versiyonunuz: Sakir { SAKIR_VERSION } " )

"" "
len (argv) (1, 3, 4) içinde değilse:
    bot.disconnect ()
Başka:
"" "
bot . run_until_disconnected ()