# Copyright (C) 2020 Sakir Usta.
#
# GPL-3.0 Lisansı altında lisanslanmıştır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# SakirUserBot - SakirBey - Midy

itibaren . ithalat  DİL , GÜNLÜKLERİ , bot , PLUGIN_CHANNEL_ID
dan  json  ithalat  yükleri , JSONDecodeError
dan  os  ithalat  yolu , kaldır
dan  Yardım kampanyasının . tl . türler  InputMessagesFilterDocument dosyasını içe  aktarır

pchannel  =  bot . get_entity ( PLUGIN_CHANNEL_ID )
KAYITLAR . bilgi ( "Dil dosyası yükleniyor ..." )
LANGUAGE_JSON  =  Yok

için  inceltilmiş  içinde  bot . iter_messages ( pchannel , filtre = InputMessagesFilterDocument ):
    eğer (( len ( dil . dosya . adı . bölme ( "." )) > =  2 ) ve ( dil . dosya . adı . bölme ( "." ) [ 1 ] ==  "sakırjson" )):
        eğer  yol . isfile ( f "./ userbot / dil / { dil . dosya . adı } " ):
            dene :
                LANGUAGE_JSON  =  yükler ( aç ( f "./ userbot / dil / { dil . Dosya . Adı } " , "r" ). Oku ())
             JSONDecodeError hariç :
                dil . sil ()
                kaldır ( f "./ userbot / dil / { dil . dosya . adı } " )

                eğer  yol . isfile ( "./userbot/language/DEFAULT.sakırjson" ):
                    KAYITLAR . uyarı ( "Varsayılan dil dosyası kullanılıyor ..." )
                    LANGUAGE_JSON  =  yükler ( açık ( f "./ userbot / dil / DEFAULT.sakırjson" , "r" ). Oku ())
                başka :
                    İstisnayı  artır ( "Dil dosyanız geçersiz" )
        başka :
            dene :
                DOSYA  =  dil . download_media ( file = "./userbot/language/" )
                LANGUAGE_JSON  =  yükler ( açık ( DOSYA , "r" ). Oku ())
             JSONDecodeError hariç :
                dil . sil ()
                eğer  yol . isfile ( "./userbot/language/DEFAULT.sakırjson" ):
                    KAYITLAR . uyarı ( "Varsayılan dil dosyası kullanılıyor ..." )
                    LANGUAGE_JSON  =  yükler ( açık ( f "./ userbot / dil / DEFAULT.sakırjson" , "r" ). Oku ())
                başka :
                    İstisnayı  artır ( "Dil dosyanız geçersiz" )
        kırmak

eğer  LANGUAGE_JSON  ==  None :
    eğer  yol . isfile ( f "./ userbot / dil / { LANGUAGE } .sakırjson" ):
        dene :
            LANGUAGE_JSON  =  yükler ( açık ( f "./ userbot / dil / { LANGUAGE } .sakırjson" , "r" ). Oku ())
         JSONDecodeError hariç :
            İstisnayı  artır ( "Geçersiz json dosyası" )
    başka :
        eğer  yol . isfile ( "./userbot/language/DEFAULT.sakırjson" ):
            KAYITLAR . uyarı ( "Varsayılan dil dosyası kullanılıyor ..." )
            LANGUAGE_JSON  =  yükler ( açık ( f "./ userbot / dil / DEFAULT.sakırjson" , "r" ). Oku ())
        başka :
            İstisnayı  artır ( f " { LANGUAGE } dosyası bulunamadı " )

KAYITLAR . bilgi ( f " { LANGUAGE_JSON [ 'LANGUAGE' ] } dili yüklendi." )

def  get_value ( eklenti  =  Yok , değer  =  Yok ):
    genel  LANGUAGE_JSON

    eğer  LANGUAGE_JSON  ==  None :
        İstisnayı  artır ( "Lütfen önce dil dosyasını yükleyin" )
    başka :
        eğer  değil  eklentisi  ==  None  veya  değer  ==  Hiçbiri :
            Eklenti  =  LANGUAGE_JSON . alın ( "STRINGS" ). get ( eklenti )
            eğer  Plugin  ==  None :
                İstisnayı  artır ( "Geçersiz eklenti" )
            başka :
                Dize  =  LANGUAGE_JSON . alın ( "STRINGS" ). get ( eklenti ). get ( değer )
                eğer  String'in  ==  None :
                    dönüş  Eklentisi
                başka :
                    dönüş  dizesi
        başka :
            İstisnayı  artır ( "Geçersiz eklenti veya dize" )