# Telif Hakkı (C) 2019 The Raphielscape Company LLC.
#
# Raphielscape Public License, Version 1.c ("Lisans") altında lisanslıdır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.
#

# SakirUserBot - SakirBey - Berce

"" "Olayları yönetmek için UserBot modülü.
UserBot'un ana bileşenlerinden biri. "" "

ithalat  sys
dan  asyncio  ithalat  create_subprocess_shell  olarak  asyncsubshell
dan  asyncio  ithalat  alt işlemi  olarak  asyncsub
dan  os  ithalat  kaldır
dan  zaman  ithalat  gmtime , strftime
dan  traceback  ithalat  format_exc
dan  teleton  ithalat  olaylar

dan  userbot  ithalat  bot , BOTLOG_CHATID , LOGSPAMMER , KALIPLARINA , SAKIR_VERSION , ForceVer


def  register ( ** değiştirgeler ):
    "" "Yeni bir etkinlik." ""
    desen  =  args . get ( 'desen' , Yok )
    disable_edited  =  args . get ( 'disable_edited' , False )
    groups_only  =  args . get ( 'groups_only' , False )
    trigger_on_fwd  =  değiştirgeler . get ( 'trigger_on_fwd' , False )
    trigger_on_inline  =  args . get ( 'trigger_on_inline' , False )
    disable_errors  =  bağımsız değişkenler . get ( 'disable_errors' , False )

    eğer  desen :
        args [ "desen" ] =  kalıp . değiştir ( "^." , "^ [" +  DESENLER  +  "]" )
    eğer  "disable_edited"  in  args :
        del  args [ 'disable_edited' ]

    eğer  "ignore_unsafe"  in  args :
        del  args [ 'ignore_unsafe' ]

    eğer  "groups_only"  in  args :
        del  args [ 'groups_only' ]

    eğer  "disable_errors"  in  args :
        del  args [ 'disable_errors' ]

    eğer  "trigger_on_fwd"  in  args :
        del  args [ 'trigger_on_fwd' ]
      
    eğer  "trigger_on_inline"  in  args :
        del  args [ 'trigger_on_inline' ]

    def  dekoratör ( func ):
        zaman uyumsuz  def  sarmalayıcı ( kontrol ):
            SiriVer  =  int ( SIRI_VERSION . Bölme ( "." ) [ 1 ])
            eğer  ForceVer  >  SiriVer :
                 kontrol bekliyorum . edit ( f "` 🌈 Botu acilen güncellemeen lazım! Bu sürüm artık kullanılamıyor..` \ n \ n __🥺 Sorunu çözmek için__ `. şimdi güncelleyin` __yazmalısın! __" )
                dönüş

            eğer  değil  LOGSPAMMER :
                send_to  =  kontrol edin . chat_id
            başka :
                send_to  =  BOTLOG_CHATID

            eğer  değil  trigger_on_fwd  ve  çek . fwd_from :
                dönüş

             kontrol ederseniz . via_bot_id  ve  trigger_on_inline değil  :
                dönüş
             
            if  groups_only  ve  check not  . is_group :
                 kontrol bekliyorum . yanıt ( "⛔ Bunun bir grup olduğunu sanmıyorum. Bu plugini bir grup dene!` " )
                dönüş

            dene :
                bekle  işlevi ( kontrol et )
                

             olaylar hariç . Yayınlamayı Durdur :
                zam  olaylar . Yayınlamayı Durdur
             KeyboardInterrupt hariç :
                geçmek
             BaseException hariç :
                eğer  değil  disable_errors :
                    tarih  =  strftime ( "% Y-% m-% d% H:% M:% S" , gmtime ())

                    eventtext  =  str ( onay . metin )
                    text  =  "** == USERBOT HATA RAPORU == ** \ n "
                    link  =  "[Sakir Destek Grubuna] (https://t.me/SakirUserBot)"
                    eğer  len ( olay metni ) < 10 :
                        text  + =  f " \ n ** 🗒️ Şu yüzden: ** { eventtext } \ n "
                    text  + =  " \ n ℹ️ İsterseniz, bunu bildirebilirsiniz."
                    text  + =  f "- sadece bu mesajı { link } gönderin. \ n "
                    text  + =  "Hata ve tarih haricinde hiçbir şey kayıt edilmez. \ n "

                    ftext  =  "========== UYARI =========="
                    ftext  + =  " \ n Bu dosya sadece burada yüklendi,"
                    ftext  + =  " \ n Sadece hata ve tarih kısmının kaydettik,"
                    ftext  + =  " \ n Gizliliğinize saygı duyuyoruz,"
                    ftext  + =  " \ n Burada herhangi bir gizli veri varsa"
                    ftext  + =  " \ n Bu hata raporu, kimse verilerinize ulaşamaz. \ n "
                    ftext  + =  "-------- USERBOT HATA GUNLUGU -------- \ n "
                    ftext  + =  " \ n Tarih:"  +  tarih
                    ftext  + =  " \ n Grup Kimliği:"  +  str ( kontrol edin . chat_id )
                    ftext  + =  " \ n Gönderen Yol Kimliği:"  +  str ( kontrol edin . sender_id )
                    ftext  + =  " \ n \ n Olay Tetikleyici: \ n "
                    ftext  + =  str ( onay . metin )
                    ftext  + =  " \ n \ n Hata metni: \ n "
                    ftext  + =  str ( sys . exc_info () [ 1 ])
                    ftext  + =  " \ n \ n \ n Geri izleme bilgisi: \ n "
                    ftext  + =  str ( format_exc ())
                    ftext  + =  " \ n \ n -------- USERBOT HATA GUNLUGU BITIS --------"
                    ftext  + =  " \ n \ n ============================== \ n "
                    ftext  + =  f "====== BOTVER: { SAKIR_VERSION } ====== \ n "
                    ftext  + =  "================================"

                    command  =  "git log --pretty = format: \" % an:% s \ " -7"

                    ftext  + =  " \ n \ n \ n Son 7 tamamlama: \ n "

                    process  =  await  asyncsubshell ( komut ,
                                                  stdout = asyncsub . BORU ,
                                                  stderr = asyncsub . BORU )
                    stdout , stderr  =  bekleme  süreci . iletişim ()
                    sonuç  =  str ( stdout . decode (). strip ()) \
                        +  str ( stderr . decode (). şerit ())

                    ftext  + =  sonuç

                    file  =  open ( "error.log" , "w +" )
                    dosya . yaz ( metin )
                    dosya . kapat ()

                    eğer  LOGSPAMMER :
                        dene :
                             kontrol bekliyorum . edit ( "" Üzgünüm, UserBot bir hatayla karşılaştı. \ n ℹ️ Hata günlükleri UserBot günlük grubunda saklanır.` " )
                        hariç :
                            geçmek
                     kontrol bekliyorum . müşteri . dosya_gönder ( send_to ,
                                                 "error.log" ,
                                                 başlık = metin )

                    remove ( "error.log" )
            başka :
                geçmek
        eğer  değil  disable_edited :
            bot . add_event_handler ( sarmalayıcı , olaylar . MessageEdited ( ** değiştirgeler ))
        bot . add_event_handler ( sarmalayıcı , olaylar . NewMessage ( ** değiştirgeler ))

        dönüş  sarıcı

    dönüş  dekoratörü