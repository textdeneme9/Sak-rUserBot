# Copyright (C) 2020 Yusuf Usta.
#
# GPL-3.0 Lisansı altında lisanslanmıştır;
# Lisans ile uyumlu olmadıkça bu dosyayı kullanamazsınız.

# SakirUserBot - Berceste - SakirBey

dan  userbot  ithalat  KALIPLARINA , CMD_HELP , CMD_HELP_BOT

sınıf  CmdHelp :
    "" "
    Komut yardımlarını daha iyi yazdığım sınıf.
    "" "

    FILE  =  ""
    ORIGINAL_FILE  =  ""
    FILE_AUTHOR  =  ""
    IS_OFFICIAL  =  Doğru
    KOMUTLAR  = {}
    ÖN EK  =  DESENLER [: 1 ]
    UYARI  =  ""
    INFO  =  ""

    def  __init__ ( öz , dosya : str , resmi : bool  =  Doğru , dosya_adı : str  =  Yok ):
        öz . FILE  =  dosya
        öz . ORIGINAL_FILE  =  dosya
        öz . IS_OFFICIAL  =  resmi
        öz . FILE_NAME  =  dosya_adı  eğer  değil  dosya_adı  ==  Yok  başka  dosya  +  '.py'
        öz . KOMUTLAR  = {}
        öz . FILE_AUTHOR  =  ""
        öz . UYARI  =  ""
        öz . INFO  =  ""

    def  set_file_info ( self , name : str , value : str ):
        eğer  adı  ==  'name' :
            öz . DOSYA  =  değer
        elif  adı  ==  'yazar' :
            öz . FILE_AUTHOR  =  değer
         kendine dön
        
    def  add_command ( self , komut : str , params  =  Yok , kullanım : str  =  '' , örnek  =  Yok ):
        "" "
        Komut ekler.
        "" "
        
        öz . KOMUTLAR [ komut ] = { 'komut' : komut , 'parametreler' : parametreler , 'kullanım' : kullanım , 'örnek' : örnek }
         kendine dön
    
    def  add_warning ( self , uyarı ):
        öz . UYARI  =  uyarı
         kendine dön
    
    def  add_info ( öz , bilgi ):
        öz . BİLGİ  =  bilgi
         kendine dön

    def  get_result ( öz ):
        "" "
        Sonuç getirir.
        "" "
        ffile  =  str ( öz . DOSYA )
        fFile  =  ffile . büyük harfle yazmak ()
        sonuç  =  f "🗂️` { fFile } `** Plugini: ** \ n "
        eğer  öz . UYARI  ==  ''  ve  öz . BİLGİ  ==  '' :
            neden  + =  "** ✨ Yetkilisi: ** f { '✅'  eğer  kendini . IS_OFFICIAL  başka  '❌' } \ n \ n "
        başka :
            neden  + =  "** ✨ Yetkilisi: ** f { '✅'  eğer  kendini . IS_OFFICIAL  başka  '❌' } \ n "
            
            eğer  öz . BİLGİ  ==  '' :
                eğer  değil  kendini . UYARI  ==  '' :
                    sonuç  + =  f "** ⚠️ Uyarı: ** { self . WARNING } \ n \ n "
            başka :
                eğer  değil  kendini . UYARI  ==  '' :
                    sonuç  + =  f "** ⚠️ Uyarı: ** { self . WARNING } \ n "
                sonuç  + =  f "** ℹ️ Bilgi: ** { self . INFO } \ n \ n "
                     
        için  komuta  içinde  kendini . KOMUTLAR :
            komut  =  öz . KOMUTLAR [ komut ]
            eğer  komut [ 'parametreler' ] ==  Yok :
                sonuç  + =  f "** 🔧 Komut: ** ' { KALIPLARI [: 1 ] } { komut [ 'komut' ] } ' \ n "
            başka :
                sonuç  + =  f "** 🔧 Komut: ** ' { KALIPLARI [: 1 ] } { komut [ 'komut' ] }  { komut [ 'parametreler' ] } ' \ n "
                
            eğer  komut [ 'örnek' ] ==  None :
                sonuç  + =  f "** 🌀 Açıklama: **" { komut [ 'kullanım' ] } " \ n \ n "
            başka :
                sonuç  + =  f "** 🌀 Açıklama: **" { komut [ 'kullanım' ] } " \ n "
                sonuç  + =  f "** 💌 Örnek: ** ' { KALIPLARI [: 1 ] } { komut [ 'örnek' ] } ' \ n \ n "
        dönüş  sonucu

    def  add ( self ):
        "" "
        Direkt olarak CMD_HELP ekler.
        "" "
        CMD_HELP_BOT [ kendi . DOSYA ] = { 'bilgi' : { 'resmi' : öz . IS_OFFICIAL , 'uyarı' : öz . UYARI , 'bilgi' : öz . BİLGİ }, 'komutlar' : öz . KOMUTLAR }
        CMD_HELP [ kendi . DOSYA ] =  öz . get_result ()
        dönüş  Doğru
    
    def  getText ( kendisi , metin : str ):
        eğer  metin  ==  'REPLY_OR_USERNAME' :
            dön  '<kullanıcı adı> <kullanıcı adı / yanıtlama>'
        elif  text  ==  'VEYA' :
            dön  'veya'
        elif  text  ==  'KULLANICI ADLARI' :
            return  '<kullanıcı ad (lar) ı>'